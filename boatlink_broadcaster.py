#!/usr/bin/env python3

# Copyright 2018 Jacques Supcik / HEIA-FR
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This program subscribes to redis messages, transform them
# and broadcast them to socket.io.
#

from flask import Flask, render_template
import flask_socketio
import redis
import logging
import json
import sys
import signal
import time
import os


def div10(x):
    return x / 10


def signed32(x):
    return x if x < (1 << 31) else x - (1 << 32)


def foil(x):
    return round((x-1024) * 360 / 4096, 2)


def temperature(x):
    return round((signed32(x)/340) + 36.53, 2)


def boatMsgBroadcaster(io):
    if 'REDIS_URL' in os.environ:
        url = os.getenv('REDIS_URL')
        logging.debug('Using Redis URL: %s', url)
        r = redis.from_url(url)
    else:
        r = redis.Redis()
    p = r.pubsub()
    p.subscribe('heiafr:hydrocontest:message')

    error1Map = {
        0: "",
        1: "Remote not connected ({0})",
        2: "Remote error ({0})",
        3: "Remote signal lost ({0})",
        -1: "Unknonw Remote Error  ({0})",
    }

    error2Map = {
        0: "",
        6: "SD Drive mount ({0})",
        7: "SD Flush ({0})",
        8: "SD Initialisation ({0})",
        9: "SD Media Detection ({0})",
        10: "SD Open ({0})",
        11: "SD Write ({0})",
        12: "SD Reset ({0})",
        -1: "Unknonw SD Error  ({0})",
    }

    trans = {
        'SpeedBoat': ('boatSpeed', lambda x: div10(x)),
        'IBatterie': ('batteryCurrent', lambda x: div10(x)),
        'CapBatteriePourcent': ('batteryPercent', lambda x: x),
        'UBatterie': ('batteryVoltage', lambda x: div10(x)),
        'SpeedMotor': ('motorSpeed', lambda x: x),
        'PositionFoil1': ('leftFoil', lambda x: foil(x)),
        'PositionFoil2': ('rightFoil', lambda x: foil(x)),
        'AccelerometreX': ('accelerometerX', lambda x: x),
        'AccelerometreY': ('accelerometerY', lambda x: x),
        'AccelerometreZ': ('accelerometerZ', lambda x: x),
        'AccelerometreDx': ('accelerometerDx', lambda x: signed32(x)),
        'AccelerometreDy': ('accelerometerDy', lambda x: signed32(x)),
        'AccelerometreDz': ('accelerometreDz', lambda x: signed32(x)),
        'GPSLontitude': ('gpsLongitude', lambda x: x),
        'GPSLatitude': ('gpsLatitude', lambda x: x),
        'PositionGouvernail': ('positionRudder', lambda x: x),
        'CapBatterieAs': ('batteryCapacity', lambda x: x),
        'PowerInst': ('instPower', lambda x: x),
        'TimeToEmptyBat': ('batteryRemainingTime', lambda x: x),
        'ConsignSpeedDriverMotor': ('motorDriverSpeedDirective', lambda x: x),
        'ConsignSpeedTelecommand': ('rcSpeedDirective', lambda x: x),
        'GPSSecond': ('gpsSecond', lambda x: x),
        'GPSMinute': ('gpsMinute', lambda x: x),
        'GPSHours': ('gpsHour', lambda x: x),
        'IMoteur': ('motorCurrent', lambda x: x),
        'UAlimCarteMoteur': ('MotorDriverVoltage', lambda x: x),
        'ConsignFoil1': ('leftFoilDirective', lambda x: foil(x)),
        'ConsignFoil2': ('rightFoilDirective', lambda x: foil(x)),
        'Temperature1': ('temperature1', lambda x: temperature(x)),
        'Temperature2': ('temperature2', lambda x: temperature(x)),
        'Temperature3': ('temperature3', lambda x: temperature(x)),
        'EmergencySTOP': ('emergencyStop', lambda x: x),
        'ModeZeroFonctionnement': ('zeroModeOperation', lambda x: x),
        'ModeManuelCourseEndurance': ('enduranceRaceDirective', lambda x: x),
        'ModeMarcheavantStopArriere': ('boatDirectionDirective', lambda x: x),
        'Error1': ('error1', lambda x: error1Map.get(x, error1Map[-1]).format(x)),
        'Error2': ('error2', lambda x: error2Map.get(x, error2Map[-1]).format(x)),
        'Error3': ('error3', lambda x: "" if x == 0 else "Erreur3 ({0})".format(x)),
        'Error4': ('error4', lambda x: "" if x == 0 else "Erreur4 ({0})".format(x)),
        'Error5': ('error5', lambda x: "" if x == 0 else "Erreur5 ({0})".format(x)),
        'reserve1': ('batteryMaxCurrent', lambda x: div10(x)),
        'reserve2': ('reserve2', lambda x: x),
        'reserve3': ('reserve3', lambda x: x),
        'reserve4': ('reserve4', lambda x: x),
    }

    while True:
        message = p.get_message()
        if message and message['type'] == "message":
            data = json.loads(message['data'].decode("utf-8"))
            msg = dict()
            for key, v in data.items():
                if key in trans:
                    t = trans[key]
                    msg[t[0]] = t[1](v)
                else:
                    msg[key] = v

            logging.debug(msg)
            io.send(msg)
        io.sleep(0.001)  # be nice to the system :)


if 'DEBUG' in os.environ:
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('socketio').setLevel(logging.INFO)
    logging.getLogger('engineio').setLevel(logging.INFO)
else:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('socketio').setLevel(logging.ERROR)
    logging.getLogger('engineio').setLevel(logging.ERROR)

app = Flask(__name__)
io = flask_socketio.SocketIO(app)
io.start_background_task(boatMsgBroadcaster, io)


def main():
    if 'PORT' in os.environ:
        port = int(os.getenv('PORT'))
        logging.debug("Listening at %d", port)
        io.run(app, port=port)
    else:
        io.run(app)


if __name__ == '__main__':
    main()
